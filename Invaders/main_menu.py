from scene import Scene
from button import Button
from PPlay.gameimage import *

class MainMenu(Scene):

  def __init__(self, world):
    self.world = world

    self.background = GameImage("images/Fundo.png")
    play_button = Button(world, 'images/btns/Play.png', 'images/btns/Play-glow.png', y=100, center_x=True, action=self.play)
    modes_button = Button(world, 'images/btns/Mode.png', 'images/btns/Mode-glow.png', y=200, center_x=True, action=self.modes_menu)
    ranking_button = Button(world, 'images/btns/Ranking.png', 'images/btns/Ranking-glow.png', y=300, center_x=True, action=self.ranking)
    quit_button = Button(world, 'images/btns/Quit.png', 'images/btns/Quit-glow.png', y=400, center_x=True, action=self.exit_game)
    self.actors = [play_button, modes_button, ranking_button, quit_button]

  def exit_game(self):
    return ('quit', True)

  def modes_menu(self):
    return ('modes_menu', True)

  def play(self):
    return ('play', True)

  def ranking(self):
    return ('ranking', True)