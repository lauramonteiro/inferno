from scene import Scene
from button import Button
from PPlay.gameimage import *

class ModesMenu(Scene):

  def __init__(self, world):
    self.world = world

    self.background = GameImage("images/Darken.png")
    easy_button = Button(world, 'images/btns/Easy.png', 'images/btns/Easy-glow.png', y=100, center_x=True, action=self.easy)
    medium_button = Button(world, 'images/btns/Medium.png', 'images/btns/Medium-glow.png', y=270, center_x=True, action=self.medium)
    hard_button = Button(world, 'images/btns/Hard.png', 'images/btns/Hard-glow.png', y=450, center_x=True, action=self.hard)

    self.actors = [easy_button, medium_button, hard_button]

  def easy(self):
    self.world.set_level('easy')
    return ('main_menu', True)

  def medium(self):
    self.world.set_level('medium')
    return ('main_menu', True)

  def hard(self):
    self.world.set_level('hard')
    return ('main_menu', True)