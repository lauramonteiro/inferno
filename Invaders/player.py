from PPlay.sprite import *
from shot import Shot

class Player(Sprite):

  def __init__(self, world):
    super().__init__('images/ship.png')
    self.world = world
    self.x = self.world.width/2 - self.width/2
    self.y = self.world.height - self.height
    self.set_position(self.x, self.y)
    self.keyboard = self.world.get_keyboard()
    self.shot_cooldown = 1
    self.last_shot = 1
    self.shots = []

  def act(self):
    self.draw()
    self.move_key_x(300*self.world.delta_time())
    self.check_wall_collision()
    self.shoot()
    for shot in self.shots:
      if shot.y < 0 - shot.height:
        self.shots.remove(shot)
        continue
      shot.act()
    return (None, None)

  def shoot(self):
    self.last_shot += self.world.delta_time()
    if self.keyboard.key_pressed('space') and self.cooldown_check():
      self.last_shot = 0
      self.shots.append(Shot(self.world, self.x + self.width/2, self.y))

  def cooldown_check(self):
    return self.last_shot >= self.shot_cooldown

  def left(self):
    return self.x + self.width
  
  def right(self):
    return self.x

  def top(self):
    return self.y

  def bottom(self):
    return self.y + self.height

  def check_wall_collision(self):
    if self.left() > self.world.width:
      self.set_position(self.world.width - self.width, self.y)
    if self.right() < 0:
      self.set_position(0, self.y)