from PPlay.gameimage import *

class Button:
  def __init__(self, window, base_image_path, hover_image_path=None, x=0, y=0, center_x=False, center_y=False, action=None):
    self.window = window
    self.base_image = GameImage(base_image_path)
    self.x = window.width/2 - self.width()/2 if center_x else x
    self.y = window.height/2 - self.height()/2 if center_y else y
    if hover_image_path:
      self.hover_image = GameImage(hover_image_path)
      self.hover_image.set_position(self.x, self.y)
    self.base_image.set_position(self.x, self.y)
    self.mouse = self.window.get_mouse()
    self.action = action

  def draw(self):
    if self.hover_image and self.is_mouse_over():
      return self.hover_image.draw()
    self.base_image.draw()

  def get_click_action(self):
    if self.action and self.clicked():
      return self.action()
  
  def is_mouse_over(self):
    return self.mouse.is_over_area((self.x, self.y), (self.x + self.width(), self.y + self.height()))

  def clicked(self):
    pressed = self.mouse.is_button_pressed(1)
    return self.is_mouse_over() and pressed

  def width(self):
    return self.base_image.width

  def height(self):
    return self.base_image.height

  def act(self):
    self.draw()
    result = self.get_click_action()
    if result:
      return result
    else:
      return (None, None)

