from PPlay.window import *

class World(Window):

  def __init__(self, width, height):
    super().__init__(width, height)
    self.level = 'easy'

  def set_level(self, level):
    self.level = level

  def set_current_scene(self, scene):
    self.current_scene = scene