from world import World
from button import Button
from scene import Scene
from main_menu import MainMenu
from modes_menu import ModesMenu
from game import Game

world = World(400, 600)
world.set_title('Space Invaders')

state = 'main_menu'

main_menu = MainMenu(world)
modes_menu = ModesMenu(world)
in_game = Game(world)
while True:
  if state == 'main_menu':
    state = main_menu.run()
  elif state == 'modes_menu':
    state = modes_menu.run()
  elif state == 'play':
    state = in_game.run()
  elif state == 'ranking':
    state = main_menu.run()
  elif state == 'quit':
    break
    
