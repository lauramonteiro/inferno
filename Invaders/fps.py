class Fps:
  def __init__(self, world):
    self.world = world
    self.timer = 0
    self.counter = 0
    self.fps = 0
    self.delta = 0

  def act(self):
      if self.timer >= 1:
        self.fps = self.counter
        self.counter = 0
        self.timer = 0
        self.world.draw_text(str(self.fps), 0, 0, color=(255, 255, 255), size=20)

      return(None, None)