class Scene:
  def __init__(self, world):
    self.world = world
    self.background = None


  def run(self):
    while True:
      break_signal = None

      if self.background:
        self.background.draw()
      for actor in self.actors:
        value, break_signal = actor.act()
        if break_signal:
          self.world.update()
          return value
      self.world.update()