from PPlay.sprite import *
from PPlay.collision import *

class Shot(Sprite):
  def __init__(self, world, x, y):
    super().__init__('images/tiro.png')
    self.world = world
    self.x = x
    self.y = y
    self.set_position(self.x, self.y)

  def act(self):
    self.set_position(self.x, self.y - 600*self.world.delta_time())
    for enemy in self.world.current_scene.enemies:
      if Collision.collided(self, enemy):
        enemy.die()
    self.draw()
