from scene import Scene
from button import Button
from player import Player
from enemy import Enemy
from fps import Fps
from PPlay.gameimage import *

class Game(Scene):

  def __init__(self, world):
    self.world = world

    self.background = GameImage("images/Darken.png")
    player = Player(self.world)
    self.enemies = [
      Enemy(self.world, x=0*self.world.width/4),
      Enemy(self.world, x=1*self.world.width/4),
      Enemy(self.world, x=2*self.world.width/4),
      Enemy(self.world, x=3*self.world.width/4),
      Enemy(self.world, x=3*self.world.width/4, level=1),
      Enemy(self.world, x=3*self.world.width/4, level=1),
      Enemy(self.world, x=3*self.world.width/4, level=1),
      Enemy(self.world, x=3*self.world.width/4, level=1),
      Enemy(self.world, x=3*self.world.width/4, level=2),
      Enemy(self.world, x=3*self.world.width/4, level=2),
      Enemy(self.world, x=3*self.world.width/4, level=2),
      Enemy(self.world, x=3*self.world.width/4, level=2),
      Enemy(self.world, x=3*self.world.width/4, level=3),
      Enemy(self.world, x=3*self.world.width/4, level=3),
      Enemy(self.world, x=3*self.world.width/4, level=3),
      Enemy(self.world, x=3*self.world.width/4, level=3),
    ]
    fps = Fps(world)

    self.actors = [player, fps]+self.enemies
    self.world.set_current_scene(self)