from PPlay.sprite import *

class Enemy(Sprite):

  def __init__(self, world, x=0, y=0, level=0):
    super().__init__('images/alien.png', 2)
    self.set_total_duration(1500)
    self.world = world
    self.level = level
    self.x = x
    self.y = self.height*self.level
    self.direction = 1
    self.steps = 0

  def act(self):
    dt = self.world.delta_time()
    if self.steps > 1500:
      self.steps = 0
      self.flip_direction()
      self.step_down()
    self.steps += 1
    self.move_x(self.direction*20*dt)
    self.steps += 1
    self.update()
    self.draw()
    return (None, None)

  def flip_direction(self):
    self.direction *= -1

  def step_down(self):
    self.level += 1
    self.y = self.height*self.level

  def die(self):
    self.world.current_scene.enemies.remove(self)
    self.world.current_scene.actors.remove(self)